<?php

namespace Sindipesca\CobrancaBundle\Doctrine\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Description of Year
 *
 * @author Luciano
 */
class Year extends FunctionNode
{
    private $string;

    public function getSql(SqlWalker $sqlWalker): string
    {
        return "YEAR(".$this->string->dispatch($sqlWalker) .")";
    }

    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        
        $this->string = $parser->StringExpression();
        
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    
}
