<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Sindipesca\CobrancaBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of DependenteRepository
 *
 */
class DependenteRepository extends EntityRepository
{

    public function getDepemdentesSemCartao()
    {
        $query = $this->createQueryBuilder("D");
        
        $query->select("D")
            ->leftJoin("D.cartoes", "CR")
            ->leftJoin("D.cliente", "C")
            ->andWhere($query->expr()->isNull("CR.id"));
        
        return $query->getQuery()->getResult();
    }
    
    public function getDepemdentesMaioridade()
    {
        $hoje = new \DateTime("now");
        $query = $this->createQueryBuilder("D");
        
        $query->select("D")
            ->innerJoin("D.cliente", "C")
            ->andWhere($query->expr()->gte("(".$hoje->format("Y")." - Year(D.nascimento))", ":idade"))
            ->orderBy("D.nascimento")
            ->setParameter("idade", 18);
        return $query->getQuery()->getResult();
    }

    
    
    
}
