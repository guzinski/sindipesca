<?php

namespace Sindipesca\CobrancaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Cartão
 *
 * @ORM\Table(name="cartao")
 * @ORM\Entity(repositoryClass="Sindipesca\CobrancaBundle\Entity\Repository\CartaoRepository")
 */
class Cartao
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
        
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    private $lote;
    
    /**
     * @var \Sindipesca\CobrancaBundle\Entity\Cliente
     *
     * @ORM\ManyToOne(targetEntity="Sindipesca\CobrancaBundle\Entity\Cliente", inversedBy="cartoes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cliente", referencedColumnName="id")
     * })
     */
    private $cliente;

    
    /**
     * @var \Sindipesca\CobrancaBundle\Entity\Dependente
     *
     * @ORM\ManyToOne(targetEntity="Sindipesca\CobrancaBundle\Entity\Dependente", inversedBy="cartoes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dependente", referencedColumnName="id")
     * })
     */
    private $dependente;
    
    
    public function __construct($lote = null, Cliente $cliente = null, \Sindipesca\CobrancaBundle\Entity\Dependente $dependente = null)
    {
        if (!is_null($lote)) {
            $this->setLote($lote);
        }
        if (!is_null($cliente)) {
            $this->setCliente($cliente);
            if (!is_null($dependente)) {
                $this->setDependente($dependente);
            }
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLote()
    {
        return $this->lote;
    }

    public function getCliente()
    {
        return $this->cliente;
    }

    public function getDependente()
    {
        return $this->dependente;
    }
    
    public function setLote($lote)
    {
        $this->lote = $lote;
        return $this;
    }

    public function setCliente(\Sindipesca\CobrancaBundle\Entity\Cliente $cliente)
    {
        $this->cliente = $cliente;
        return $this;
    }

    public function setDependente(\Sindipesca\CobrancaBundle\Entity\Dependente $dependente)
    {
        $this->dependente = $dependente;
        return $this;
    }


    
    
    
    
    
}
