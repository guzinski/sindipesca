<?php

namespace Sindipesca\CobrancaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Conveniada
 *
 * @ORM\Table(name="conveniada")
 * @ORM\Entity()
 */
class Conveniada implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    
    /**
     * @var string
     * @Assert\NotBlank(message="Razão Social deve ser preenchida")
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $razaoSocial;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Nome Fantasia deve ser preenchido")
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $nomeFantasia;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Responsável deve ser preenchido")
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $responsavel;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telefone;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telefone2;

    /**
     * @var string
     * @Assert\NotBlank(message="Endereço deve ser preenchido")
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $endereco;

    /**
     * @var string
     * @Assert\Email(message = "Insira um e-mail válido.")
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="senha", type="string", length=150, nullable=false)
     */
    private $senha;

    public function getId()
    {
        return $this->id;
    }

    public function getRazaoSocial()
    {
        return $this->razaoSocial;
    }

    public function getNomeFantasia()
    {
        return $this->nomeFantasia;
    }

    public function getResponsavel()
    {
        return $this->responsavel;
    }

    public function getTelefone()
    {
        return $this->telefone;
    }

    public function getEndereco()
    {
        return $this->endereco;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function setRazaoSocial($razaoSocial)
    {
        $this->razaoSocial = $razaoSocial;
        return $this;
    }

    public function setNomeFantasia($nomeFantasia)
    {
        $this->nomeFantasia = $nomeFantasia;
        return $this;
    }

    public function setResponsavel($responsavel)
    {
        $this->responsavel = $responsavel;
        return $this;
    }

    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
        return $this;
    }

    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;
        return $this;
    }

    public function eraseCredentials()
    {
        
    }

    public function getPassword()
    {
        return $this->getSenha();
    }

    public function getRoles()
    {
        return array('ROLE_CONSULTA');
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->getEmail();
    }

    public function getTelefone2()
    {
        return $this->telefone2;
    }

    public function setTelefone2($telefone2)
    {
        $this->telefone2 = $telefone2;
        return $this;
    }


    
}
