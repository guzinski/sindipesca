<?php

namespace Sindipesca\CobrancaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Divida
 *
 * @ORM\Table(name="divida", indexes={@ORM\Index(name="FK_divida_cliente", columns={"id_cliente"})})
 * @ORM\Entity(repositoryClass="Sindipesca\CobrancaBundle\Entity\Repository\DividaRepository")
 */
class Divida
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vencimento", type="date", nullable=false)
     */
    private $vencimento;

    /**
     * @var float
     *
     * @ORM\Column(name="valor", type="float", precision=10, scale=0, nullable=false)
     */
    private $valor;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var \Sindipesca\CobrancaBundle\Entity\Cliente
     *
     * @ORM\ManyToOne(targetEntity="Sindipesca\CobrancaBundle\Entity\Cliente", inversedBy="dividas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cliente", referencedColumnName="id")
     * })
     */
    private $cliente;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany (targetEntity="Sindipesca\CobrancaBundle\Entity\Negociacao", mappedBy="divida")
     */
    private $negociacoes;

    
    public function __construct()
    {
        $this->setNegociacoes(new \Doctrine\Common\Collections\ArrayCollection());
    }

        /**
     * Set vencimento
     *
     * @param \DateTime $vencimento
     * @return Divida
     */
    public function setVencimento($vencimento)
    {
        $this->vencimento = $vencimento;

        return $this;
    }

    /**
     * Get vencimento
     *
     * @return \DateTime 
     */
    public function getVencimento()
    {
        return $this->vencimento;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Divida
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set cliente
     *
     * @param \Sindipesca\CobrancaBundle\Entity\Cliente $cliente
     * @return Divida
     */
    public function setCliente(\Sindipesca\CobrancaBundle\Entity\Cliente $cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Sindipesca\CobrancaBundle\Entity\Cliente 
     */
    public function getCliente()
    {
        return $this->cliente;
    }
    
    public function __toString()
    {
        return (string) $this->getValor();
    }

    /**
     * 
     * @return \Doctrine\Common\Collections\Collection $negociacoes
     */
    function getNegociacoes()
    {
        return $this->negociacoes;
    }

    /**
     * 
     * @param \Doctrine\Common\Collections\Collection $negociacoes
     * @return \Sindipesca\CobrancaBundle\Entity\Divida
     */
    function setNegociacoes(\Doctrine\Common\Collections\Collection $negociacoes)
    {
        $this->negociacoes = $negociacoes;
        return $this;
    }


    
}
