<?php

namespace Sindipesca\CobrancaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of ConveniadaType
 *
 * @author Luciano
 */
class ConveniadaType extends AbstractType
{
    
    public function getName()
    {
        return 'conveniada';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("nomeFantasia")
                ->add("razaoSocial", 'text', ['label'=>'Razão Social'])
                ->add("responsavel", 'text', ['label'=>'Responsável'])
                ->add("endereco",  'text', ['label'=>'Endereço'])
                ->add("email", 'email', ['label'=>'E-mail'])
                ->add("telefone")
                ->add("telefone2");
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(['data_class' => 'Sindipesca\CobrancaBundle\Entity\Conveniada']);
    }

    
}
