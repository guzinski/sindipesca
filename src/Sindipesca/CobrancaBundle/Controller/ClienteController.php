<?php

namespace Sindipesca\CobrancaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sindipesca\CobrancaBundle\Entity\Cartao;
use Sindipesca\CobrancaBundle\Entity\Cliente;
use Sindipesca\CobrancaBundle\Form\ClienteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\Exception;
use ZipArchive;

/**
 * Description of ClienteController
 *
 * @author Luciano
 */
class ClienteController extends Controller
{
    /**
     * @Route("/clientes", name="clientes")
     * @Template()
     */
    public function indexAction() 
    {
        $cidades = $this->getDoctrine()
                ->getRepository(Cliente::class)
                ->getCidades();
        return ['cidades'=>$cidades];
    }
    
    /**
     * @Route("/clientes/pagination", name="clientes_pagination")
     * @Template()
     */
    public function paginationAction(Request $request)
    {
        $firstResult    = $request->query->getInt("start");
        $maxResults     = $request->query->getInt("length");
        $busca          = $request->get("search");
        $ordem          = $request->get("order");
        $cidade         = $request->get("cidade");
        $categoria      = $request->get("categoria");

        $repClientes = $this->getDoctrine()
                            ->getRepository("Sindipesca\CobrancaBundle\Entity\Cliente");
        $clientes = $repClientes->getClientes(
                $busca['value'], 
                $maxResults, 
                $firstResult, 
                $ordem,
                $cidade,
                $categoria);
        
        $dados = array();
        foreach ($clientes as $cliente) {
            if ($cliente->getTipo()==Cliente::ASSISTENCIAL) {
                $link = $this->generateUrl("cadastro_assistencial", array("id"=>$cliente->getId()));
            } elseif ($cliente->getTipo()==Cliente::AMADOR) {
                $link = $this->generateUrl("cadastro_amador", ["id"=>$cliente->getId()]);
            } elseif ($cliente->getTipo()==Cliente::PROFISSIONAL) {
                $link = $this->generateUrl("cadastro_profissional", ["id"=>$cliente->getId()]);
            }
            $dados[] = [
                "<h5><a href=\"".$link."\">". $cliente->getNome()."</a></h5>",
                $cliente->getTelefone(),
                $cliente->getEndereco(),
                $cliente->getBairro(),
                $cliente->getCidade(),
                $cliente->getRegistro(),
                Cliente::getTipoEnumValues()[$cliente->getTipo()],
            ];
        }
        $return = [
            'draw' => $request->get("draw"),
            'recordsTotal' => $repClientes->count(),
            'recordsFiltered' => count($clientes),
            'data' => $dados,
        ];
        return new Response(json_encode($return));
    }
 
    /**
     * @Route("/cadastro/assistencial/{id}", name="cadastro_assistencial")
     * @Template()
     * 
     * @param Request $request
     * @return RedirectResponse
     */
    public function assistencialAction(Request $request, $id=0)
    {
        return $this->cadastro($request, $id, Cliente::getTipoEnumValues()[Cliente::ASSISTENCIAL]);
    }
    
    /**
     * @Route("/cadastro/amador/{id}", name="cadastro_amador")
     * @Template()
     * 
     * @param Request $request
     * @return RedirectResponse
     */
    public function amadorAction(Request $request, $id=0)
    {
        return $this->cadastro($request, $id, Cliente::getTipoEnumValues()[Cliente::AMADOR]);
    }
    
    /**
     * @Route("/cadastro/profissional/{id}", name="cadastro_profissional")
     * @Template()
     * 
     * @param Request $request
     * @return RedirectResponse
     */
    public function profissionalAction(Request $request, $id=0)
    {
        return $this->cadastro($request, $id, Cliente::getTipoEnumValues()[Cliente::PROFISSIONAL]);
    }
    
    
    private function cadastro(Request $request, $id=0, $tipo) 
    {
        $em = $this->getDoctrine()->getManager();
        if ($id>0) {
            $cliente = $em->find("Sindipesca\CobrancaBundle\Entity\Cliente", $id);
        } else {
            $cliente = new Cliente();
        }

        $arquivos = clone $cliente->getArquivos();
        $dependentes = clone $cliente->getDependentes();
        $form = $this->createForm(new ClienteType(), $cliente, array('em'=>$em));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $cliente->setTipo(strtoupper($tipo));
            $this->get("cliente")->salvarCliente($cliente, $arquivos, $dependentes);                        
            return new RedirectResponse($this->generateUrl('clientes'));
        }
        
        return ['form'=>$form->createView(), 'cliente'=>$cliente, 'tipo'=>$tipo];
    }

    /**
     * @Route("/cliente/arquivos", name="cliente_arquivos")
     * 
     * @Template
     * @param int $idParcela
     * @return Response
     */
    public function listaDocumentosAction(Request $request)
    {
        $id = $request->get("id", null);
        if ($id==null) {
            throw new Exception("Cliente não foi encontrado");
        }

        $cliente = $this->getDoctrine()->getRepository("Sindipesca\\CobrancaBundle\\Entity\\Cliente")->find($id);
        if ($cliente==null) {
            throw new Exception("Cliente não foi encontrado");
        }
        return array("cliente"=>$cliente);
    }
    
    /**
     * @Route("/cliente/download/{nome}", name="cliente_arquivo_download")
     * 
     * @param int $nome Nome do Arquivo
     * @return Response
     */
    public function downloadArquivoAction($nome = null)
    {
        if (is_null($nome)) {
            throw new NotFoundHttpException;
        }
        if (file_exists($this->get('kernel')->getRootDir() . '/../web/uploads/arquivos/'.$nome)) {
            return $this->get("upload")->download($this->get('kernel')->getRootDir() . '/../web/uploads/arquivos/'.$nome);
        } elseif (file_exists($this->get('kernel')->getRootDir() . '/../web/uploads/temp/'.$nome)) {
            return $this->get("upload")->download($this->get('kernel')->getRootDir() . '/../web/uploads/temp/'.$nome);
        } else {
            throw new NotFoundHttpException;
        }
    }
    
    /**
     * @Route("/cliente/carta/aniversario/{nome}/{tipo}", name="cliente_carta")
     * 
     * @Template
     * @param int $idParcela
     * @return Response
     */
    public function cartaAction($nome, $tipo)
    {
         return array("clienteNome"=> $nome, "tipo" => $tipo);
    }
    
    
    /**
     * @Route("/cliente/gerar/cartao", name="cliente_gerar_cartao")
     * 
     * @return Response
     */
    public function gerarCartoes()
    {
        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository("Sindipesca\CobrancaBundle\Entity\Cliente")->getClientesSemCartao();
        $dependentes = $em->getRepository("Sindipesca\CobrancaBundle\Entity\Dependente")->getDepemdentesSemCartao();
        $lote = (int) $em->getRepository("Sindipesca\CobrancaBundle\Entity\Cartao")->getNumeroLote();

        $lote++;
        $cartoes = [];
        
        foreach ($clientes as $cliente) {
            $cartao = new Cartao($lote, $cliente);
            $em->persist($cartao);
            $cartoes[] = $cartao;
        }
        foreach ($dependentes as $dependente) {
            $cartao = new Cartao($lote, $dependente->getCliente(), $dependente);
            $em->persist($cartao);
            $cartoes[] = $cartao;
        }
        
        $em->flush();        
        
        $zip = new ZipArchive();
        $filename = $this->get('kernel')->getRootDir()."/../web/uploads/temp/".time().".zip";
        
        if ($zip->open($filename, ZIPARCHIVE::CREATE)!==TRUE) {
            exit("cannot open <$filename>\n");
        }
        
        $csvName = time();
        $output = fopen($this->get('kernel')->getRootDir()."/../web/uploads/temp/".$csvName.".zip", 'w');

        fputcsv($output, array('Numero', 'Nome', 'Tipo', 'Dependente'), ";");
        $fs = new \Symfony\Component\Filesystem\Filesystem();
        foreach ($cartoes as $cartao) {
            if ($cartao->getDependente()) {
                $imgname = $this->get('kernel')->getRootDir()."/../web/uploads/arquivos/".$cartao->getDependente()->getFoto();
                if (file_exists($imgname) && !is_dir($imgname)) {
                    $file = new File($imgname);
                } else {
                    $file = null;
                }
            } else {
                $imgname = $this->get('kernel')->getRootDir()."/../web/uploads/arquivos/".$cartao->getCliente()->getFoto();
                if (file_exists($imgname) && !is_dir($imgname)) {
                    $file = new File($imgname);
                } else {
                    $file = null;
                }
            }
            $numero = $cartao->getId();
            if ($file != null) {
                $zip->addFile($file->getPath()."/".$file->getFilename(), $numero.".".$file->getExtension());
            }
            fputcsv($output, array($numero, $cartao->getCliente()->getNome(), $cartao->getCliente()->getTipo(), $cartao->getDependente() ? $cartao->getDependente()->getNome() : ""), ';');
        }
        fclose($output);
        $zip->addFile($this->get('kernel')->getRootDir()."/../web/uploads/temp/".$csvName.".zip", "planilha.csv");
        $zip->close();
                
        $response = new Response(file_get_contents($filename));
        $file = new File($filename);
        $response->headers->set('Content-Type', $file->getMimeType());
        $response->headers->set("Content-Disposition", "attachment; filename=cartoes.zip");
        $response->headers->set("Content-Length", $file->getSize());
        
        return $response;
    }

    /**
     * @Route("/cliente/aniversariantes", name="clientes_anivesariantes")
     */
    public function aniversariantesAction(Request $request)
    {
        $hoje = new \DateTime("now");
        $clientes = $this->getDoctrine()->getRepository("Sindipesca\CobrancaBundle\Entity\Cliente")->getAniversariantes($hoje->format("m"), $hoje->format("d"));
        $dados = array();
        foreach ($clientes as $cliente) {
            $link = $this->generateUrl("cliente_carta", ["nome"=>$cliente->getNome(), "tipo"=> $cliente->getTipo()]);
            $dados[] = [
                "<h5>".$cliente->getNome()."</h5>",
                $cliente->getTelefone(),
                $cliente->getCidade(),
                $cliente->getNascimento()->format('d/m'),
                Cliente::getTipoEnumValues()[$cliente->getTipo()],
                "<a class='btn btn-default btn-sm' href=\"".$link."\">Imprimir</a>",
            ];
        }
        $return = [
            'draw' => $request->get("draw"),
            'recordsTotal' => count($clientes),
            'recordsFiltered' => count($clientes),
            'data' => $dados,
        ];
        return new Response(json_encode($return));

    }
    
    
    
    /**
     * @Route("/clientes/exportar", name="clientes_exportar")
     * @param Request $request
     * @return StreamedResponse
     */
    public function exportarAction(Request $request)
    {        
        $response = new \Symfony\Component\HttpFoundation\StreamedResponse();
        $response->setCallback(function() use ($request) {
            $handle = fopen('php://output', 'w+');
            fputs( $handle, "\xEF\xBB\xBF" );
            // Add the header of the CSV file
            fputcsv($handle, array('Nome', 'Telefones', 'Cidade', 'Tipo De cadastro'),';');
            
            $firstResult    = $request->query->getInt("start");
            $maxResults     = $request->query->getInt("length");
            $busca          = $request->get("search");
            $ordem          = $request->get("order");
            $cidade         = $request->get("cidade");
            $categoria      = $request->get("categoria");

            $repClientes = $this->getDoctrine()
                                ->getRepository("Sindipesca\CobrancaBundle\Entity\Cliente");
            $clientes = $repClientes->getClientes(
                    $busca['value'], 
                    $maxResults, 
                    $firstResult, 
                    $ordem,
                    $cidade,
                    $categoria);
            
            foreach ($clientes as $cliente) {
                fputcsv(
                    $handle, 
                    [
                        $cliente->getNome(),
                        $cliente->getTelefone(),
                        $cliente->getCidade(),
                        Cliente::getTipoEnumValues()[$cliente->getTipo()],
                    ],
                    ';' 
                );                
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Encoding', 'UTF-8');
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="clientes.csv"');
        
        return $response;
    }

    
}
